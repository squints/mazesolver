package mazesolver;
import java.util.*;

/**
 * Generic Tree data structure and some useful methods.
 * 
 * @author camilletaylor
 * @param <T> accepts generic type objects
 */
public class Tree<T> {

    private List<Tree> children = new ArrayList<>();
    private Tree parent;
    private T direction;
    private T cell;

    /**
     *
     * @param compass cardinal direction
     * @param destination cell on pseduogrid
     */
    public Tree(T compass, T destination) {
        this.direction = compass;
        this.parent = null;
        this.cell = destination;
    }

    /**
     *
     * @param compass cardinal direction
     * @param destination cell on pseudogrid
     * @param parent parent tree node (previous cell)
     */
    public Tree(T compass, T destination, Tree parent) {
        this.direction = compass;
        this.parent = parent;
        this.cell = destination;
        parent.addChild(this);
    }

    /**
     *
     * @return this cell's parent
     */
    public Tree getParent() {
        return parent;
    }

    /**
     *
     * @return all children of this cell
     */
    public List<Tree> getChildren() {
        return children;
    }

    /**
     *
     * @param parent resets parent of this cell
     */
    public void setParent(Tree parent) {
        parent.addChild(this);
        this.parent = parent;
    }

    /**
     * Detaches this tree from its parent
     */
    public void orphanize() {
        this.parent = null;
    }

    /**
     *
     * @param child resets/adds orphaned children
     */
    public void addChild(Tree child) {
        //     child.setParent(this);
        this.children.add(child);
    }

    /**
     *
     * @param root top of tree: staring position
     * @param value searches by value for cell or direction
     * @return the node that is found
     */
    public Tree bfs(Tree root, T value) {
        Queue<Tree> queue = new LinkedList<>();
        queue.add(root); //add the top of the tree
        while (!queue.isEmpty()) {
            Tree currentnode = queue.poll(); //get first node in the queue
            if (currentnode.getCell().toString().equals(value.toString())
                    || currentnode.getDirection().toString().equals(value.toString())) { //if the node matches then return that node
                return currentnode;
            }
            if (currentnode.getChildren() != null) {
                currentnode.getChildren().stream().forEach((nodes) -> { //(in parallel), add all children in the queue 
                    queue.add((Tree) nodes);
                });
            }
        }
        return null;
    }

    /**
     *
     * @param node destination node
     * @return the depth from this node to requested node in method
     */
    public int getDepthTo(Tree node) {
        if (node.getParent() != null) {
            return 1 + node.getDepthTo(node.getParent());
        } else {
            return 0;
        }
    }
    
    /**
     * 
     * @param node destination node
     * @return a List<> of Trees that contain the path from this node to the requested destination
     */
    public List<Tree> getPathTo(Tree node) {
        List<Tree> path = new ArrayList<>();
        path.add(this);
        Tree currentnode = this;
        while (currentnode.getParent() != null)
        {
            currentnode = currentnode.getParent(); //keep adding parents of the node
            path.add(currentnode);
        }
        Collections.reverse(path); //current list is backwards (from target to source), need to be flipped.
        return path;
    }

    /**
     *
     * @return the direction from parent node to this node
     */
    public T getDirection() {
        return this.direction;
    }

    /**
     *
     * @return cell this node represents
     */
    public T getCell() {
        return this.cell;
    }
}
