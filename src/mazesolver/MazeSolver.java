/**
 *
 * @author camilletaylor
 */
package mazesolver;
/**
 *
 * @author camilletaylor
 */
public class MazeSolver {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // maze space encoded by hand (see attached write-up)
        Tree node1 = new Tree<String>("Start","L2");
        Tree node2 = new Tree<String>("West", "K2", node1);
        
        Tree node3 = new Tree<String>("North", "L1",node1);
        Tree node4 = new Tree<String>("West", "K1", node3);
        Tree node5 = new Tree<String>("West", "J1", node4);
        
        Tree node6 = new Tree<String>("West", "I1", node5);
        Tree node7 = new Tree<String>("South", "I2", node6);
        Tree node8 = new Tree<String>("South", "I3", node7);
        Tree node9 = new Tree<String>("West", "H3", node8);
        Tree node10 = new Tree<String>("South", "H4", node9);
        
        Tree node11 = new Tree<String>("South", "J2", node5);
        Tree node12 = new Tree<String>("South", "J3", node11);
        Tree node13 = new Tree<String>("East", "K3", node12);
        Tree node14 = new Tree<String>("East", "L3",node13);
        Tree node15 = new Tree<String>("South", "L4",node14);
        Tree node16 = new Tree<String>("South", "L5",node15);
        Tree node17 = new Tree<String>("West", "K5",node16);
        Tree node18 = new Tree<String>("West", "J5", node17);
        Tree node19 = new Tree<String>("South", "K6", node17);
        Tree node20 = new Tree<String>("East", "L6", node19);        
        
        Tree node21 = new Tree<String>("West", "K4", node15);
        Tree node22 = new Tree<String>("West", "J4", node21);
        Tree node23 = new Tree<String>("West", "I4", node22);
        Tree node24 = new Tree<String>("South", "I5", node23);
        Tree node25 = new Tree<String>("West", "H5", node24);
        Tree node26 = new Tree<String>("West", "G5", node25); 
        Tree node27 = new Tree<String>("South", "G6", node26);
        Tree node28 = new Tree<String>("South", "G7", node27);
        
        Tree node29 = new Tree<String>("North", "G4", node26);
        Tree node30 = new Tree<String>("North", "G3", node29);
        Tree node31 = new Tree<String>("North", "G2", node30);
        Tree node32 = new Tree<String>("East", "H2", node31);
        Tree node33 = new Tree<String>("North", "H1", node32);
        Tree node34 = new Tree<String>("West", "G1", node33);
        Tree node35 = new Tree<String>("West", "F1", node34);
        Tree node36 = new Tree<String>("West", "E1", node35);
        Tree node37 = new Tree<String>("West", "D1", node36);
        Tree node38 = new Tree<String>("West", "C1", node37);
        Tree node39 = new Tree<String>("South", "C2", node38);
        Tree node40 = new Tree<String>("East", "D2", node39);
        Tree node41 = new Tree<String>("South", "D3", node40);
        Tree node42 = new Tree<String>("South", "D4", node41);
        Tree node43 = new Tree<String>("East", "E4", node42);
        
        Tree node44 = new Tree<String>("South", "F2", node35);
        Tree node45 = new Tree<String>("West", "E2", node44);
        Tree node46 = new Tree<String>("South", "E3", node35);
        Tree node47 = new Tree<String>("East", "F3", node46);
        Tree node48 = new Tree<String>("South", "F4", node47);
        Tree node49 = new Tree<String>("South", "F5", node48);
        Tree node50 = new Tree<String>("South", "F6", node49);
        Tree node51 = new Tree<String>("South", "F7", node50);
        Tree node52 = new Tree<String>("West", "E7", node51);
        Tree node53 = new Tree<String>("North", "E6", node52);
        Tree node54 = new Tree<String>("North", "E5", node53);
        Tree node55 = new Tree<String>("West", "D5", node54);
        Tree node56 = new Tree<String>("South", "D6", node55);
        Tree node57 = new Tree<String>("South", "D7", node56);
        Tree node58 = new Tree<String>("South", "D8", node57);
        Tree node59 = new Tree<String>("South", "D9", node58);
        Tree node60 = new Tree<String>("South", "D10", node59);
        Tree node61 = new Tree<String>("South", "D11", node60);
        
        Tree node62 = new Tree<String>("West", "C8", node58);
        Tree node63 = new Tree<String>("West", "B8", node62);
        Tree node64 = new Tree<String>("North", "B7", node63);
        Tree node65 = new Tree<String>("North", "B6", node64);
        Tree node66 = new Tree<String>("North", "B5", node65);
        Tree node67 = new Tree<String>("North", "B4", node66);
        
        Tree node68 = new Tree<String>("North", "C7", node62);
        Tree node69 = new Tree<String>("North", "C6", node68);
        Tree node70 = new Tree<String>("North", "C5", node69);
        Tree node71 = new Tree<String>("North", "C4", node70);
        Tree node72 = new Tree<String>("North", "C3", node71);
        Tree node73 = new Tree<String>("West", "B3", node72);
        Tree node74 = new Tree<String>("North", "B2", node73);
        Tree node75 = new Tree<String>("North", "B1", node74);
        Tree node76 = new Tree<String>("West", "A1", node75);
        Tree node77 = new Tree<String>("South", "A2", node76);
        Tree node78 = new Tree<String>("South", "A3", node77);
        Tree node79 = new Tree<String>("South", "A4", node78);
        Tree node80 = new Tree<String>("South", "A5", node79);
        Tree node81 = new Tree<String>("South", "A6", node80);
        Tree node82 = new Tree<String>("South", "A7", node81);
        Tree node83 = new Tree<String>("South", "A8", node82);
        Tree node84 = new Tree<String>("South", "A9", node83);
        Tree node85 = new Tree<String>("South", "A10", node84);
        Tree node86 = new Tree<String>("South", "A11", node85);
        Tree node87 = new Tree<String>("South", "A12", node86);
        Tree node88 = new Tree<String>("East", "B12", node87);
        Tree node89 = new Tree<String>("North", "B11", node88);
        Tree node90 = new Tree<String>("North", "B10", node89);
        Tree node91 = new Tree<String>("North", "B9", node90);
        Tree node92 = new Tree<String>("East", "C9", node91);
        Tree node93 = new Tree<String>("South", "C10", node92);
        Tree node94 = new Tree<String>("South", "C11", node93);
        Tree node95 = new Tree<String>("South", "C12", node94);
        Tree node96 = new Tree<String>("East", "D12", node95);
        Tree node97 = new Tree<String>("East", "E12", node96);
        Tree node98 = new Tree<String>("East", "F12", node97);
        Tree node99 = new Tree<String>("East", "G12", node98);
        Tree node100 = new Tree<String>("East", "H12", node99);
        Tree node101 = new Tree<String>("East", "I12", node100);
        Tree node102 = new Tree<String>("East", "J12", node101);
        Tree node103 = new Tree<String>("East", "K12", node102);
        
        Tree node104 = new Tree<String>("North", "H11", node100);
        Tree node105 = new Tree<String>("North", "H10", node104);
        Tree node106 = new Tree<String>("West", "G10", node105);
        Tree node107 = new Tree<String>("West", "F10", node106);
        Tree node108 = new Tree<String>("South", "F11", node107);
        Tree node109 = new Tree<String>("West", "G11", node108);
        
        Tree node110 = new Tree<String>("North", "F9", node107);
        Tree node111 = new Tree<String>("East", "G9", node110);
        Tree node112 = new Tree<String>("East", "H9", node111);
        Tree node113 = new Tree<String>("East", "I9", node112);
        Tree node114 = new Tree<String>("East", "J9", node113);
        Tree node115 = new Tree<String>("North", "J8", node114);
        Tree node116 = new Tree<String>("East", "K8", node115);
        Tree node118 = new Tree<String>("South", "K9", node116);
        
        Tree node119 = new Tree<String>("West", "I8", node115);
        Tree node120 = new Tree<String>("North", "I7", node119);
        
        Tree node121 = new Tree<String>("North", "E11", node97);
        Tree node122 = new Tree<String>("North", "E10", node121);
        Tree node123 = new Tree<String>("North", "E9", node122);
        Tree node124 = new Tree<String>("East", "E8", node123);
        Tree node125 = new Tree<String>("East", "F8", node124);
        Tree node126 = new Tree<String>("East", "G8", node125);
        Tree node127 = new Tree<String>("East", "H8", node126);
        Tree node128 = new Tree<String>("North", "H7", node127);
        Tree node129 = new Tree<String>("North", "H6", node128);
        Tree node130 = new Tree<String>("East", "I6", node129);
        Tree node131 = new Tree<String>("East", "J6", node130);
        Tree node132 = new Tree<String>("South", "J7", node131);
        Tree node133 = new Tree<String>("East", "K7", node132);
        Tree node134 = new Tree<String>("East", "L7", node133);
        Tree node135 = new Tree<String>("South", "L8", node134);
        Tree node136 = new Tree<String>("South", "L9", node135);
        Tree node137 = new Tree<String>("South", "L10", node136);
        Tree node138 = new Tree<String>("West", "K10", node137);
        Tree node139 = new Tree<String>("West", "J10", node138);
        Tree node140 = new Tree<String>("West", "I10", node139);
        Tree node141 = new Tree<String>("South", "I11", node140);
        Tree node142 = new Tree<String>("East", "J11", node141);
        Tree node143 = new Tree<String>("East", "K11", node142);
        Tree node144 = new Tree<String>("East", "L11", node143);
        Tree node145 = new Tree<String>("Finish", "L12", node144);
        
        //find and retrieve the "Finish" node 
        Tree finish = node1.bfs(node1, "Finish");
        
        System.out.println("Finding Finish from Start using BFS: " + finish.getCell());
        System.out.println("Steps from Start to Finish (Min. Depth): " + node1.getDepthTo(finish));
                
        System.out.println("\n--Tracing steps--\n");
        System.out.println("By cells:");
        System.out.print("(Start)");
        finish.getPathTo(node1).stream().forEach((node) -> {
            System.out.print(((Tree) node).getCell() + " -> ");
        }); //get path from Start to Finish
        System.out.println("(Finish)\n");
        
        
        System.out.println("By Direction:");
        finish.getPathTo(node1).stream().forEach((node) -> {
            System.out.print(((Tree) node).getDirection() + " -> ");
        });
        System.out.println("Done");
        
        System.out.println("\n--CODE COMPLETE--\n");
    }
    
}
